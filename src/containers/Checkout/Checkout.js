import React from 'react';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import {Route, Redirect} from 'react-router-dom';
import ContactData from '../../components/Order/ContactData/ContactData';
import { connect } from 'react-redux';
import Modal from '../../components/UI/Modal/Modal';

const Checkout = props => {
    
    const checkoutCancelHandler = () => {
        props.history.goBack();
    }

    const checkoutCantinueHandler = () => {
        props.history.replace('checkout/contact-data');
    }

    const modalClosedHandler = () => {
        props.history.replace('/');
    }

    
    let summary = <Redirect to='/' />
    if (props.ings) {
        summary = (
            <div>
                <CheckoutSummary 
                    ingredients={props.ings}
                    checkoutCanceled={checkoutCancelHandler}
                    checkoutContinued={checkoutCantinueHandler} />
                <Route 
                    path={props.match.url + '/contact-data'} 
                    component={ContactData} />

                <Modal show={props.purchased} modalClosed={modalClosedHandler}>
                    <p style={{textAlign: "center", fontWeight: "bold"}}>THANKS FOR ORDERING!</p>
                        <p style={{textAlign: "center"}}> We`ll contact you! </p>
                </Modal>
            </div>
            )
    };

    return summary;
}

const mapStateToProps = state => {
    return {
        ings: state.burger.ingredients,
        purchased: state.order.purchased
    }
}


export default connect (mapStateToProps) (Checkout);