import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import classes from './Auth.module.css';
import Input from '../../components/UI/Input/Input';
import Spinner from '../../components/UI/Spinner/Spinner';
import Button from '../../components/UI/Button/Button';
import * as actions from '../../store/actions/index';
import { updateObject } from '../../shared/utility';
import { checkValidity } from '../../shared/validation';

const Auth = props => {
   const [controls, setControls] = useState({
        email: {
            elementType: 'input',
            elementConfig: {
                type: 'email',
                placeholder: 'Email Address'
            },
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password: {
            elementType: 'input',
            elementConfig: {
                type: 'password',
                placeholder: 'Password'
            },
            value: '',
            validation: {
                required: true,
                minLength: 6
            },
            valid: false,
            touched: false
        }
    })

    const [isSignup, setIsSignUp] = useState(true)

    const inputChangedHandler = ( event, controlName ) => {
        const updatedControls = updateObject(controls, {
            [controlName]: updateObject(controls[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, controls[controlName].validation),
                touched: true
            }) 
        })

        setControls(updatedControls)
    }

    const { building, authRedirectPath, onSetAuthRedirectPath } = props

    useEffect(() => {
        if (!building && authRedirectPath !== '/') {
            onSetAuthRedirectPath();
        }
    },[building, authRedirectPath, onSetAuthRedirectPath])


    const submitHandler = (event) => {
        event.preventDefault();
        props.onAuth(controls.email.value, controls.password.value, isSignup)
    }

    const switchAuthMethodHandler = () => {
        setIsSignUp(!isSignup)
    }

    const formElementsArray = [];
    for ( let key in controls ) {
        formElementsArray.push({
            id: key,
            config: controls[key]
        })
    }

    let form = (
        <form onSubmit={submitHandler} >
            {formElementsArray.map(elem => (
                <Input 
                key={elem.id}
                elementType={elem.config.elementType} 
                elementConfig={elem.config.elementConfig}
                value={elem.config.value} 
                changed={(event) => inputChangedHandler(event, elem.id)}
                invalid={!elem.config.valid}
                shouldValidate={elem.config.validation}
                touched={elem.config.touched} />
            ))}

            <Button btnType="Success">SUBMIT</Button>
        </form>  
    );

    if (props.loading) {
        form = <Spinner/>
    }

    let errorMessage = null;

    if (props.error) {
        errorMessage = (
        <p>{props.error.message}</p>
        )
    }

    let Redir = null;

    if (props.isAuth) {
        Redir = <Redirect to={props.authRedirectPath} />
    }

    return (
        <div className={classes.Auth}>
            <h1>{isSignup ? 'Sign In' : 'Sign Up'}</h1>
            { Redir }
            {errorMessage}
            {form}
        <Button
            clicked={switchAuthMethodHandler} 
            btnType="Danger">Switch to {isSignup ? 'Sign Up' : 'Sign In'}</Button>  
        </div>
    );
};

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuth: state.auth.token !== null,
        building: state.burger.building,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: ( email, password, isSignup ) => dispatch(actions.auth( email, password, isSignup )),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);